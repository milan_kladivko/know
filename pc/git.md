
Two accounts accessing the same domain
======================================

[Gitlab has its docs.](https://docs.gitlab.com/ee/user/ssh.html)
There's 2 ways to do it: 

- A `git config` command to force a specific SSH command with the identity file we need. 
- Aliasing the domain and using it with `git clone`, while writing the alias-identfile pair in `.ssh/config`. 


Using `git` without help from client wrappers
==================================================

Staging all deletions
--------------------------------

> If you have a mix of modified and deleted files and only want to stage 
> deleted files to the index, you can use `git ls-files` as a filter.

https://stackoverflow.com/q/43161383

    git ls-files --deleted | xargs git add


Modifying the remote origin
------------------------------------

> Usually, I clone things with `https` and then I need to set it to `ssh`
> because I don't want to put in the long password all the time. 

```sh 
# To print the currently-set remote
git remote -v

# To set HTTPS
git remote set-url origin https://github.com/USERNAME/REPOSITORY.git
# To set SSH
git remote set-url origin     git@github.com:USERNAME/REPOSITORY.git
```

> **Be careful when copypasting!**
> There is a `.com:` for SSH and `.com/` for HTTPS. 
