
> Trying to wrap up everything I need for work so that
> I don't have to install a single thing in my life again
> and reuse the same thing regardless of the computer / OS I'm on. 
> 
> The editor, bashrc, installed system packages, the OS tweaks, 
> WM, work folder with my files maybe.


Collecting static binaries for everything I need for work
=============================================================

**Yeah right, good luck with that in the Linux world!!!** \
No, not happening. Sorry. 



Using a VM and syncing it
=============================================

## PROS
- the most likely-to-work-nicely way to do it
- stupidly simple to sync
- proven by time
- no installations again anywhere
- VM files are just that -- files (even single-files)
- VM files 
- works even if I jump on a Windows machine!! (gaming)

## CONS
- potentially huge -- entire OS (~20GB)
- performance issues, maybe (don't care on gaming rigs)
- access to hardware (running and exposing a webserver)
- needs 2 OSes running at the same time (1+7GB RAM min)
- dunno how access to hardware resources is gonna work (qmk flashing, supposedly possible tho)

## What VM software to use 

### vmware
- (-) not open source (as if I care)
- (-) I don't know it as well
- (+) hardware acceleration 
- (+) liked by many, i guess

### virtualbox
- (-) shit performance
- (-) supposedly, doesn't do hardware acceleration of graphics well
- (-) outdated af
- (-) usb issues and other integration issues, maybe
- (+) used most by windows normies => nice support
- (+) open source
- (+) should always work, right?

### boxes
- (+) supposedly best performance (kernel blabla)
- (+) has a flatpak (might be broken)
- (-) linux only
- (-) [BUG, can't use it](https://gitlab.gnome.org/GNOME/gnome-boxes/-/issues/877)

## Resources

*syncing*
- https://www.google.com/search?q=vm+sync+entire+machine+reddit
- https://superuser.com/questions/1065888/how-can-i-share-a-virtual-machine-between-computers-using-something-like-dropbox
- https://serverfault.com/questions/374116/synchronize-vmware-virtual-machine
- https://www.google.com/search?q=vm+sync+entire+machine+site:superuser.com
- https://superuser.com/questions/589096/maintain-and-sync-a-virtualbox-vm-between-two-separate-computers
- https://www.google.com/search?q=drbd&oq=drbd
- https://www.google.com/search?q=unison+linux+sync

*vm software*
- https://www.google.com/search?q=vmware+vs+virtualbox
- https://www.google.com/search?q=vmware+vs+virtualbox+2022+reddit
- https://www.reddit.com/r/AskNetsec/comments/8ujyi2/vmware_or_virtualbox/
- https://www.reddit.com/r/Kalilinux/comments/nxhqbe/guys_i_just_wanna_askwhich_one_better_vmware_or/
- https://www.reddit.com/r/virtualbox/comments/k8s3d8/virtualbox_or_vmware_player/


Distrobox [has a file](./distrobox.md)
====================================
Using `docker`/`podman` for not-quite-VM setup to containerize it. 

## PROS
- pretty good on a Steam Deck
- can export executables to the host system 
  - just kidding -- doesn't work half the time
- pretty small footprint on the host system
## CONS
- doesn't sync well -- gotta genarate snapshots and move them whole
  - loading the snapshots [doesn't work anyway](https://github.com/89luca89/distrobox/issues/560)
- surprisingly, doesn't do the same thing on every machine (bugs differ between machines)


Setting up on a new computer
=================================

## .bashrc edits (history that doesn't suck and so on)
```

```

## basic binaries
```
micro 
ranger
python 3
```

## git config edits to start with
```
git config
```


