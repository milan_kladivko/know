

```sh

# purge all .cache files that have not been accessed in 100 days:
find ~/.cache/ -type f -atime +100 -delete 

# Journal folder -- `/var/log/journal/`
journalctl --vacuum-size=50M
journalctl --vacuum-time=2weeks

# Manjaro -- pamac
pamac clean
# Everyone else -- pacman package cache
sudo pacman -Scc
# "Orphan" package cleanup
sudo pacman -Rs $(pacman -Qdtq)

```
