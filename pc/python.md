
# installing python projects
- get the code from git
- cd into the folder

### first :: do venv
`python -m venv ./venv`

### second :: source activate into the venv
`source ./venv/bin/activate`

### third :: get dependencies for the project
`pip install -r requirements.txt`
> Sometimes, requirements install will fail with weird errors. 
> Make sure you run the install with the `venv` activated,
> sometimes it literally cannot be installed without it. 


# python virtual environments

> Remember, it's all stupid dumb crap. \
> Python is terrible. \
> Don't let that drag you down. 

[not a bad overview for all the ways to do it](https://www.freecodecamp.org/news/manage-multiple-python-versions-and-virtual-environments-venv-pyenv-pyvenv-a29fb00c296f/)

## When using the same version as system

[official docs](https://docs.python.org/3/tutorial/venv.html)

### To create a venv
`python -m venv ./dir-to-put/venv/into`
- This way, you can supply a different python version and it'll 
  act like the only python on the system. Useful. 

### To use a venv
`source ./dir-to-put/venv/into/bin/activate`
- That's a lot of writing, innit?
- Use some aliases to not have to do this all the time.


## When specific outdated version is necessary

...sigh... we have to do some `.bashrc` edits. \
For some reason, it's not as simple as one would like. 

- [github](https://github.com/pyenv/pyenv#installation)

- [installer](https://github.com/pyenv/pyenv-installer)

run the installer, a oneliner 
```curl https://pyenv.run | bash```

as per the instructions in this oneliner install, 
add stuff to terminal init somehow
```
# Python pyenv (including all the stuff that's necessary)
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PATH:$PYENV_ROOT/bin"
eval "$(pyenv init -)"
# Load pyenv-virtualenv automatically by adding
# the following to ~/.bashrc:
eval "$(pyenv virtualenv-init -)"
```

- [uninstall](https://github.com/pyenv/pyenv-installer#uninstall)

remove the folder with all the pyenv related installed data
```rm -rf ~/.pyenv```

then remove these three lines from `.bashrc`...
```
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"
```



