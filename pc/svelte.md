
Tooling
=================

## Ignore warnings when compiling svelte

```ts

const ignore_warnings = [
    // We use mixins for things, and we might not use every case
    // described by the mixin. 
    "css-unused-selector",
    // Sometimes, configuration variables of a component aren't used. 
    "unused-export-let", 
    // We don't care about any accessibility stuff, really.
    "a11y-click-events-have-key-events",

    // Anchor with "target=_blank" should have "rel=noreferrer",
    // but I don't care.
    "security-anchor-rel-noreferrer",
    // I use tabindex to have it under control, but I guess that's
    // illegal usage... But again, don't care as long as it works.
    "a11y-no-noninteractive-tabindex", 
]

	plugins: [
        svelte({
            onwarn(warning, defaultHandler) {
                if (ignore_warnings.includes(warning.code))
                    return
                
                defaultHandler(warning)
                console.log("code=[", warning.code, "]")
            },
        })
    ],
```
