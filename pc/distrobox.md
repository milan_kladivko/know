
Sources of Information
==========================================

[article with the basics](https://www.ianwootten.co.uk/2022/11/30/how-to-setup-distrobox-on-the-steam-deck/)

The Steps
=============

- I made it a single bash script thing so I can copypaste the whole goddamn thing. 

## Add into bashrc
```
# Distrobox -- Coding in an Environment Container
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$HOME/.local/podman/bin
# TODO: Prevent it from doing whacky shit on every terminal open. 
xhost +SI:localuser:$USER
```

## Installations

```sh

# Installed distrobox and podman (I think it'll work anywhere)
# ( needs sudo password )
curl -s https://raw.githubusercontent.com/89luca89/distrobox/main/install | sh -s -- --prefix ~/.local
curl -s https://raw.githubusercontent.com/89luca89/distrobox/main/extras/install-podman | sh -s -- --prefix ~/.local
# Created an "image" -- going for the most straightforward and supported distro
distrobox create -i ubuntu:22.10
# Entered into the image. Now I can access ubuntu stuff. 
distrobox enter ubuntu-22-10 

# cant even add apt repositories
sudo apt install   software-properties-common
# Installed some basics that are apt-able immediatelly
sudo apt install   meld git curl  nano micro  ranger
# Downloaded the vscode official deb and installed it. 
# ( TODO:  Do it purely in the terminal!! )
cd Desktop/
curl -o code.deb -L http://go.microsoft.com/fwlink/?LinkID=760868
sudo apt install ./code.deb
rm code.deb 

# Installed `nvm` so I can use different versions of NodeJS for work
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash 


```



Issues
========

`systemctl` not usable
-------------

https://linuxhandbook.com/system-has-not-been-booted-with-systemd/

