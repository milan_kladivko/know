
From [mycli github](https://github.com/dbcli/mycli/issues/269)
[Explanation](https://explainshell.com/explain?cmd=ssh+-N+-f+user%40host+-L+3306%3Alocalhost%3A3306)

-----

Something like to set up the tunnel:

`ssh -Nf <ssh-user>@<ssh-host> -L localhost:3306:<remote-mysql-host>:3306`

Then connect to mysql as if it was running on localhost:

`mycli -h localhost -P 3306`

It's not specific to mycli, you'd use the same with mysql.

