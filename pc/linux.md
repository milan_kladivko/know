
# Executable dynamic linkage
> What does the executable need from the system to be installed? 

`file <executable>` ... what "kind" of file is it (ranger prints this when previewing)

`ldd <executable>`  ... if "dynamic", what does it need? (libc etc)
