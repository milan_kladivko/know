
# Opening up SSH

https://shendrick.net/Gaming/2022/05/30/sshonsteamdeck.html
`sudo systemctl start sshd` 
... probably works on everything w/out installs, not just a steam deck

# Getting a Folder Size 
`du -sh /folder`

# `scp` -- the ssh-equivalent of `cp`
- might not do links correctly
- might not do perms correctly
- not sure if I can "resume" it
**In other words, don't bother, use `rsync` instead**


# Rsync

> Works nicely with ssh, though you need a parallel ssh to tab-complete
> to the correct place and copypaste between the terminals.

## Doing it "well enough"

- `rsync -vah` for a bunch of small files, shows filenames
- `rsync -Pah` for a bunch of bigger files, shows speed


## Doing it at speed (many huge files)

https://superuser.com/questions/109780/how-to-speed-up-rsync

If you're using rsync with a fast network or disk to disk in the same machine,
- not using compression `-z`
  - compression uses lots of CPU
- and using `--inplace`
  - not using inplace makes the harddrive thrash alot
  - it uses a temp file before creating the final
- `--no-compress` might speed you up. 
- `-W` to copy files whole
  - always use this if you don't want it to compare differences 
  - never mind that the point of rsync is to only update the changes
... speeds it up to the performance of the harddrives or network. 

Compression & not using --inplace is better for doing it over the internet (slow network)

NEW: Be aware of the destination... if there is NTFS "compression" enabled... this severely slows down large files (I'd say 200MB+) rsync almost seems stalled, it's caused by this.

My command would be:
`rsync -avAXEWSlHh /src /dest --no-compress --info=progress2 --dry-run`
- If all looked well, I'd delete `--dry-run` and let it go. 
- `A`, `X`, and `E` cover extended attributes and permissions not covered by `-a`.
- `l` is for soft links
- `H` is for hard links
- `h` is for human readable

https://superuser.com/questions/1296297/why-is-scp-faster-than-rsync

If needed, `--append` is another way to resume an interrupted file transfer without causing a delta-transfer.


# Syncthing

# KDE Connect
