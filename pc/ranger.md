
# Ranger -- TUI file manager

## Drag and Drop

[Ranger Wiki](https://github.com/ranger/ranger/wiki/Drag-and-Drop)

Install `dragon`. 

    yay -S dragon-drop

Add this line into `~/.config/ranger/rifle.conf`. 

    has dragon, X, flag f = dragon -a -x "$@"

**...And then be pissed that it doesn't work anyway.**

