# K9 - Mail for Android
https://f-droid.org/en/packages/com.fsck.k9/
# Mail for Desktop
https://gitlab.com/muttmua/mutt
https://www.rainloop.net/
https://www.thunderbird.net/en-US/


# Gitea - self hosted git frontend (github.com alternative), `Go`
https://gitea.io/

# Gource - animate work being done over time as graph
https://gource.io/

# Rustdesk - videocalling, `Rust`
https://github.com/rustdesk/rustdesk

# lf - Ranger but in Go 
https://github.com/gokcehan/lf
https://larbs.xyz/lf/
...and without tabs and paging hahaha

# GeoGebra - desmos offline
https://wiki.geogebra.org/en/Reference:GeoGebra_Installation
... look for the portable version -- it has one for linux


# Gamepad Tester - website to test gamepad input
https://gamepad-tester.com/


# Exporting Calendar events to an Excel file
https://blog.coupler.io/export-google-calendar-to-google-sheets/#Can_I_export_Google_Calendar_to_a_spreadsheet_with_native_functionality


# Zenity - make GTK dialogs for bash scripts 
https://en.wikipedia.org/wiki/Zenity

# gitk - GUI git log wrapper for browsing history
https://www.atlassian.com/git/tutorials/gitk


## renderdoc
> OpenGL doesn't tell you anything when something goes wrong
> so having something to inspect runtime graphics errors
> is essential. 
`Rouge Legacy 2` -- Jblow \
https://www.youtube.com/watch?v=XM0_fG4iGvE


# Shebang -- Run Go as a Script
```
///usr/bin/true; exec /usr/bin/env go run "$0" "$@"
package main
import "fmt"
func main() { fmt.Println("你好！") }
```
https://stackoverflow.com/a/30082862
> I have tested this answer with bash, dash, zsh, and ksh.
