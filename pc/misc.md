
# "Log in required" -> "OK" webpage

Network manager attempts to ping http://www.archlinux.org/check_network_status.txt but that seems to be unavailable because doing a curl -i gives me a 301 redirect. Plasma is just overriding it.

What you can do temporarily is edit 
`/usr/lib/NetworkManager/conf.d/20-connectivity.conf` 
and set it to point to the kde endpoint

    [connectivity]
    #uri=http://www.archlinux.org/check_network_status.txt
    uri=http://networkcheck.kde.org/
    
Then just bounce NetworkManager

    systemctl restart NetworkManager.service

