
> Sorted by order of execution in an install scenario. 


# Bashrc history setting

http://mywiki.wooledge.org/BashFAQ/088
```sh
HISTFILESIZE=400000000
HISTSIZE=10000
PROMPT_COMMAND="history -a"
shopt -s histappend
```

# Essential git repositories

```

```

## Git defaults
https://spin.atomicobject.com/2020/05/05/git-configurations-default/
```sh 
# Don't pollute the history with pointless merges
git config --global pull.rebase true

# Set an email and username
git config --global user.email kladivko.dev@gmail.com
git config --global user.name  Milan Kladivko
```


# Installations

## Static binaries

## Host Distro

```
pacman -S bash-completion base-devel
```

## Distrobox

```
```



# Lowercasing default home folder names

https://wiki.archlinux.org/index.php/XDG_user_directories

========

# Work computer, PHP project distrobox debian supercommands

```
sudo apt install software-properties-common nano git curl fuse ncdu unzip bash-completion 
sudo apt install python2 python3

sudo apt install emacs ranger xclip kitty meld
sudo apt install postgresql php-cli php-curl php-gd php-imap php-zip php-xml php-imagick php-dom php-bcmath php-pgsql php-mbstring
```

