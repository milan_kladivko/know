Distrobox -- Virtualizing the setup 
============================================

Just check out the [distrobox page](./distrobox.md)



Installing packages -- Readonly Filesystem
============================================================

> Installing anything with `pacman` requires disabling the read-only 
> btrfs protection of the system. 

- Anything outside the user directory will be wiped, including `pacman` installs. 
- Set up install scripts to set everything up after an update. 
- Don't forget to update that script with every system-level modification. 
- Prefer Flatpaks and other means to have a fully static and portable linux software. 
- Set up syncthing for things that change often. 
- KDE's `Discover Software Center` uses Flatpaks, so use that most of the time. 

## [Here's the process:](https://www.reddit.com/r/SteamDeck/comments/t8al0i/install_arch_packages_on_your_steam_deck/)

- The OP process -- maybe outdated and a little too involved: 
  - If you have not already, use `passwd` to create a password for the `deck` user.
  - Disable read-only mode: `sudo btrfs property set -ts / ro false`
  - Initialize the pacman keyring: `sudo pacman-key --init`
  - Populate the pacman keyring with the default Arch Linux keys: `sudo pacman-key --populate archlinux`
  - Try installing a package: `sudo pacman -S emacs`
  
- `sudo btrfs property set -ts / ro false` 
  - This should be the official way to do it
  - [Here's a guide](https://help.steampowered.com/en/faqs/view/671A-4453-E8D2-323C)


## Unofficial static binaries for things

### NodeJS
- https://github.com/a-sync/nodejs.org

- Should look at `bun` for running nodejs stuff though... 

### MySQL
- The client -- `mysql` executable -- is static. 
- But I want to use `mycli` for that haha. 

- Server might be harder but totally possible -- it's all open source. 




Pacman Installs (Wacom Tablet GUI, CoreCtrl/VR, ...)
================

By default, the Steam Deck doesn't have the package that enables configuration GUI in KDE. 
To enable it, we have to mess with the immutable install and get a package. 

> NOTE: When reinstalled, [some configs might be broken](https://www.reddit.com/r/linuxquestions/comments/ugp84q/steamos_update_messed_with_libwacom_gui_install/)
> [and some other things](https://www.reddit.com/r/linuxquestions/comments/uaslo6/how_to_access_drawing_tablet_parameters_in_steam/)

> We'll be ignoring any keyring shite, I'll just disable any
> security checksum(?) checking that I don't care about.
> I just want to install a thing, don't care about security. 
> If I can just download an executable file off the net,
> why are we acting like this is an issue with package managers.

> NOTE: On OS Update, this package will be wiped and will need to be reinstalled. 

```sh
# Disable OS immutability
sudo steamos-readonly disable
# ...set `SigLevel = TrustAll` in that file
# needs sudo so no VSCode, just go nano
sudo nano /etc/pacman.conf
# If you still get an error, try this (as noted in the error)
# I guess even if we're not using the key, we still to make one
sudo pacman-key --init

# Run the installs, whatever you need.
sudo pacman -S kcm-wacomtablet corectrl yabridge yabridgectl realtime-privileges

# Enable OS immutability, just in case
sudo steamos-readonly enable
# ...set `SigLevel = Required DatabaseOptional` back
sudo nano /etc/pacman.conf
```


USB C
=======


https://www.reddit.com/r/explainlikeimfive/comments/b99uzz/eli5_the_difference_between_usb_30_usb_31_typec/

```
USB is a protocol standard: it defines how both connected devices can communicate with each other. Newer generations of the USB protocol allow for faster speeds:

- USB 2.0: 500 Mbit / second
- USB 3.0 = USB 3.1 gen 1 = USB 3.2 gen 1 : 5 Gbit / second
- USB 3.1 gen 2 = USB 3.2 gen 2 : 10 Gbit / second
- USB 3.2 gen 2 x 2 : 20 Gbit / second

The naming scheme is ridiculous, indeed. You'll see this speed difference when you transfer large files, but when hooking up a printer or mouse it isn't noticeable.

Type C is a connector type. It defines the lay-out of the port. For example Type A (= classic USB port) needs to be connected in the correct orientation, whereas type C ports also fit "upside-down".

Thunderbolt is another protocol standard. USB is specifically designed to connect office devices and some file transfers. Thunderbolt on the other hand can do pretty much everything. (This is due to the direct connection to the super fast PCIe lanes on the motherboard.) You can use it as a display output, Ethernet connection or even attach an external graphics card. Thunderbolt also uses the type C connector.
```
