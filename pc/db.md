

# Tools
## Go -- `sqlc` (when not in control of schema)
## Go -- `sqlboiler` (simple crud, schema is mine)

# SQL 

## Dumping -- `mysqldump`

https://www.sqlshack.com/how-to-backup-and-restore-mysql-databases-using-the-mysqldump-command/

Base command (`>` for generating out)
```sh
mysqldump -u [user] –p  [options] [database_name] [tablename] > [dumpfilename.sql]
```
> NOTE: Don't write the password here -- it'll ask. 

- `--no-create-info` no schema
- `--no-data` no data


Applying a dump (`<` for reading in)
```sh 
mysql -u [user] –p [pass]  < [dumpfilename.sql]
```


# Postgres

## Setup the basics: First user and database

```sh
sudo su - postgres # "login" as the postgres admin user
pgsql # run as that admin (passwords might not work, they're empty->safety)
```

> This doesn't work for example, we need to actually get into the `pgsql` console
> as an admin/sudo user and create it from there.
> Or at least, that's necessary from a Distrobox Steamdeck running Debian.
> ```
> > sudo -u postgres psql
> Sorry, user deck is not allowed to execute '/usr/bin/createuser' as postgres on work.steamdeck.
> ```

**Note: In the Postgres Console, `"` and `'` are different and not interchangeable.**

**Note: You would usually look into the `.env` file of your project to see what the names of user/pass/db should be.**

```pgsql
-- create a new user
create user "wolf-waagen-user";
-- set the password for the user
alter user "wolf-waagen-user" with encrypted password 'secret';
-- create a database 
create database "wolf-waagen-db";
-- give access for db to the user
grant all privileges on database "wolf-waagen-db" to "wolf-waagen-user";
```


## "could not change directory to --- permission denied"

**Just move out to the root dir and do stuff there. It's not worth caring about.**

> I needed to create a database and fill it with data. 
> Couldn't access the data -- didn't have perms because everything
> in Postgres must be done through the `postgres` user. 
> 
> Some say, you just need `x` perms on the directory. 
> I say that is bullshit -- didn't work. 

## Setup

> I should have some stuff written from setting up Wolf postgres already,
> but I don't know where to look...

Honestly, just use some wrapper software like podman, docker 
or distrobox to not have to install like a dumbass. 

