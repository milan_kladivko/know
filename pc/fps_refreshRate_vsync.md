
## Firefox
```
URL:     about:config
Setting: layout.frame_rate
Value:   120 (has to be hardcoded, set to -1 when done)
```

## VSCode 
```
Find the desktop file (or whatever)
Add '--use-gl=egl'
Start VSCode
```
```
Note: Will disable hardware acceleration, supposedly??
Note: There is "disable-hardware-acceleration" 
    for 'Preferences: Configure Runtime Arguments'
    but that did not work for me
```
