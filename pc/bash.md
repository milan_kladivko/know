
# Bash

## Never lose bash history again!!
http://mywiki.wooledge.org/BashFAQ/088

Write this into bash: 
```sh
HISTFILESIZE=400000000
HISTSIZE=10000
PROMPT_COMMAND="history -a"

shopt -s histappend
```

And there is a lot of other things that you can do to not lose it. 
Not going into it right now though (archiving). 

## Where's the default .bashrc??

`cp /etc/skel/.bashrc ~/.bashrc.default`
