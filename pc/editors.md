# Emacs

## Shortcuts 

### Default positions for cut/copy/paste -- `cua-mode` 

- If C-x is needed, it goes through only when a selection is not chosen. 
  Otherwise, it will use the selection for cut. 
  
- It blocks C-DEL behavior -- the word is not cut as is the Emacs default. 
  That is both good and bad, very subjective.


## Dired as a Tree View (and glance-sidebar)

  https://github.com/emacsorphanage/direx
  https://emacs.stackexchange.com/questions/413/tree-based-directory-browser


## "Directory local variables"

Pretty much the only way to edit settings for LSP per-project.
https://endlessparentheses.com/a-quick-guide-to-directory-local-variables.html
It's basically just a file that you can paste into directories
and Emacs reads the file and edits the variables whenever the buffer
is in the directory. Not sure how exactly it works but it sounds dope.

