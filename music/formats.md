

# ffmpeg conversion to Opus/.ogg

https://www.reddit.com/r/ffmpeg/comments/po4c72/mp3_to_opus_best_quality/
```sh
# Alternative qualities: 
# -vn -c:a libopus -b:a  80k "${f}_80k.ogg"  
# -vn -c:a libopus -b:a  128k "${f}_128k.ogg"
 
for f in *.mp3; do
ffmpeg -i "$f" -vn -c:a libopus -b:a  96k "${f}_96k.ogg" 
done 

for f in *.flac; do   
ffmpeg -i "$f" -vn -c:a libopus -b:a  96k "${f}_96k.ogg"
done 

echo "------  DONE  ------"
```


# ffmpeg compression for screen recordings on call

> This wouldn't work for anything that isn't a kinda-pointless
> videocall where some people just talk and nothing is happening
> on the screen like ever.
> But that's exactly what 20GB of acceptably-compressed video
> of 200MB per hour footage looks like lol


Here's  
[a gist](https://gist.github.com/lukehedger/277d136f68b028e22bed)
where I found the general lines, and a discussion for it.

```sh 
ffmpeg   \
-i 2024-12-06\ 13-48-08\ ww\ chris.mp4   \
-vcodec h264 -b:v 700k -b:a 96k -acodec mp3   \
2024-12-06\ 13-48-08\ ww\ chris.tiny.mp4
```

Where the command is just input, options, output. The codec options might be redundant, but it's there as the expected format.

It also takes really goddamn long to do anything, but that's just how it is, isn't it. The PC will be chugging pretty hard, but all that work doesn't stop the computer from doing anything that's not gaming or music production, so I don't really care. The code editor works fine throughout this. 



## Kinda big still

```sh
ffmpeg   \ 
-i 2024-12-06\ 13-48-08\ ww\ chris.mp4   \
-s 1280x720 -acodec copy -y   \
2024-12-06\ 13-48-08\ ww\ chris.tiny.mp4
========
Output #0, mp4, to '2024-12-06 13-48-08 ww chris.tiny.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf60.16.100
  Stream #0:0(und): Video: h264 (avc1 / 0x31637661), yuv420p(tv, bt709, progressive), 1280x720 [SAR 1:1 DAR 16:9], q=2-31, 60 fps, 15360 tbn (default)
    Metadata:
      handler_name    : VideoHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libx264
    Side data:
      cpb: bitrate max/min/avg: 0/0/0 buffer size: 0 vbv_delay: N/A
  Stream #0:1(und): Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 192 kb/s (default)
    Metadata:
      handler_name    : SoundHandler
      vendor_id       : [0][0][0][0]
[out#0/mp4 @ 0x55a6ea2ce940] video:306678kB audio:209561kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 2.689587%
frame=534382 fps=321 q=-1.0 Lsize=  530123kB time=02:28:26.32 bitrate= 487.6kbits/s speed=5.34x    
```


## Bigger than before, wtf
```sh
ffmpeg   \
-i 2024-12-06\ 13-48-08\ ww\ chris.mp4   \
-b:v 700k   -b:a 96k -acodec mp3   \
2024-12-06\ 13-48-08\ ww\ chris.tiny.mp4
========
Output #0, mp4, to '2024-12-06 13-48-08 ww chris.tiny.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf60.16.100
  Stream #0:0(und): Video: h264 (avc1 / 0x31637661), yuv420p(tv, bt709, progressive), 1280x720 [SAR 1:1 DAR 16:9], q=2-31, 700 kb/s, 60 fps, 15360 tbn (default)
    Metadata:
      handler_name    : VideoHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libx264
    Side data:
      cpb: bitrate max/min/avg: 0/0/700000 buffer size: 0 vbv_delay: N/A
  Stream #0:1(und): Audio: mp3 (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 96 kb/s (default)
    Metadata:
      handler_name    : SoundHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libmp3lame
[out#0/mp4 @ 0x55f23810fa00] video:653003kB audio:104372kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 1.713637%
frame=534382 fps=296 q=-1.0 Lsize=  770353kB time=02:28:26.32 bitrate= 708.6kbits/s speed=4.93x  
```



## 15fps (webcam and screenshare framerate is low), 420p video, 96k,mp3 sound for speech only
```sh
ffmpeg  \
-i in.mp4   \
-b:a 96k -acodec mp3 \
-filter:v "fps=15, scale=-2:420"   \
out.mp4
========
Output #0, mp4, to 'out.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf60.16.100
  Stream #0:0(und): Video: h264 (avc1 / 0x31637661), yuv420p(tv, bt709, progressive), 746x420 [SAR 1120:1119 DAR 16:9], q=2-31, 15 fps, 15360 tbn (default)
    Metadata:
      handler_name    : VideoHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libx264
    Side data:
      cpb: bitrate max/min/avg: 0/0/0 buffer size: 0 vbv_delay: N/A
  Stream #0:1(und): Audio: mp3 (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 96 kb/s (default)
    Metadata:
      handler_name    : SoundHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libmp3lame
[out#0/mp4 @ 0x55c03201b600] video:17672kB audio:17660kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 1.572889%
frame=22605 fps=288 q=-1.0 Lsize=   35888kB time=00:25:06.96 bitrate= 195.1kbits/s speed=19.2x 
```

## and ogg instead of mp3 for the sound ... ? fuckin sound cut down to HALF, WHAT
```sh
ffmpeg   \
-i in.mp4   \
-b:a 48k -c:a libvorbis   \
-filter:v "fps=15, scale=-2:420"   \
out.mp4
========
Output #0, mp4, to 'out.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf60.16.100
  Stream #0:0(und): Video: h264 (avc1 / 0x31637661), yuv420p(tv, bt709, progressive), 746x420 [SAR 1120:1119 DAR 16:9], q=2-31, 15 fps, 15360 tbn (default)
    Metadata:
      handler_name    : VideoHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libx264
    Side data:
      cpb: bitrate max/min/avg: 0/0/0 buffer size: 0 vbv_delay: N/A
  Stream #0:1(und): Audio: vorbis (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 48 kb/s (default)
    Metadata:
      handler_name    : SoundHandler
      vendor_id       : [0][0][0][0]
      encoder         : Lavc60.31.102 libvorbis
[out#0/mp4 @ 0x55bd1edec600] video:17672kB audio:7494kB subtitle:0kB other streams:0kB global headers:4kB muxing overhead: 3.625356%
frame=22605 fps=269 q=-1.0 Lsize=   26078kB time=00:25:06.94 bitrate= 141.8kbits/s speed=17.9x    
```



## Going through an entire folder of recordings and compressing each file

```sh
for f in *_comp.mp4; do
ffmpeg   \
-i "$f"   \
-b:a 48k -c:a libvorbis   \
-filter:v "fps=15, scale=-2:420"   \
"compressed/$f.tiny.mp4"
done 
```
