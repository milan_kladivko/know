

# Making Windows VSTs work on Linux

We need:
- Yabridge
    - Uses Wine to provide VSTs to a DAW.
    - Needs a specific new Wine Staging version, nothing works if versions aren't compatible.
    - You can install this with the distro package manager, we'll need to use it anyway.
- Wine
    - Translates Windows system calls to Linux ones, thus providing Windows compatibility.
- Bottles
    - Provides any Wine versions you might need from any place.
    - Installation is through Flatpak, no need to mess around.
    - Manages prefixes, so that the Installed Files are contained.
- VST plugins
    - Find some that are obviously working fine and don't have complicated installs or managers.
- Host distro `realtime-privileges` package
    - This might need some investigation, distro work is always more complicated.

## Install Packages and programs

- Bottles flatpak; Discover store or equivalent has got you covered.
- Flatseal flatpak, to allow Bottles more File Access

- Yabridge `yabridgectl`
    - they just have a static folder of binaries, make sure it's in the PATH and you're set
    - you can also do distro package manager install, but you'll have an extra systemwide `wine` installed, boooooo
- `realtime-privileges`
    - this almost certainly needs a distro install, scary name for a package to be raw-dogging

## Bottles setup

- Some program-wide setups
    - Set the folder for Bottles, it shouldn't be in a stupid place, you might need it in home.
        - The default might be fine, the Flatpak has data in `.var/app/***`, figure out what the default is, maybe it's a good default.
    - Install a Runner/Loader of the correct Wine version for Yabridge
        - Open up yabridge github, click open the github version tag of the release you're using, and look into the readme -- they have a "tested with Wine Staging version" section that lists the version.
        - Open up the Bottles Preferences, find something with that number, and install it.

- Gotta set up an instance...
    - Start with an empty instance
    - Set the Runner/Loader we installed earlier
    - Set dxvk
    - Maybe look into the DPI - it has to be set per-instance


## Add entries to `~/.bashrc`

```sh
# Yabridge - DAW plugins
# ... this exposes the yabridgectl command and all libs it needs
export PATH=$PATH:$HOME/.local/share/yabridge
# ... and this exposes the `wine` command that the Bottle should be using
#     but do make sure that it's the correct one here, that it exists etc
export PATH=$PATH:$HOME/.var/app/com.usebottles.bottles/data/bottles/runners/kron4ek-wine-9.19-staging-amd64/bin/
```

## Installing VSTs

- Download your EXEs that you need
- To run them, either
    - open up Bottles app > Your bottle > Run with Bottle > find it in the Filesystem
    - open up the Filesystem > Rightclick menu > Run with Bottles
    - doubleclick might just open up the Bottles app as well
- And hopefully, it just goes fine.

## Synchronizing VSTs with yabridge

- Open up a terminal
- run `yabridgectl status` - it should be in your PATH variable
    - it'll tell you if yabridge is missing its files
- Open up the Filesystem, look for your Bottle (or "see files" in Bottles app)
- Copy paths like "Program Files" and "Common Files" and so on
    - include anything that might have VST exe or dll files in it
- run `yabridgectl sync`
    - if the wine version isn't correct, now it'll fail, with a descriptive message about it
    - it might run some installations of `mono` or whatever, install those too
- You should see a rundown of VSTs found+added+known.

## Opening up a DAW prints "memlock" warnings

> If yabridge prints errors or warnings about memory locking limits,
> then that means that you have not yet set up realtime privileges for your user.
> Setting the memlock limit to unlimited (or -1) is usually part of this process.
> How you should do this will depend on your distro.

So this might be the only part that needs actual OS distro installation.
Make sure you have `realtime-privileges` or `realtime-setup` package installed.
```sh
sudo gpasswd -a "$USER" realtime
```
And then we gotta reboot.


# Possible issues

- Bottles won't open a folder when I need to see it
    - Go into OS settings, default apps, and re-set the choice there. It's just failing on opening the default file manager.

- How do I know what version of Wine to use
    - Open up yabridge github, click open the github version tag of the release you're using, and look into the readme -- they have a "tested with Wine Staging version" section that lists the version.
