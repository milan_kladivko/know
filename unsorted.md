
`Compositor is Evil` \
https://raphlinus.github.io/ui/graphics/2020/09/13/compositor-is-evil.html

`LZW Compression` -- library safety vs. custom impl

It's not safer to use libraries than doing your own.
It will be much safer to support the -necessary- subset
of features for the issue than to risk the enormous code 
surface of a more "complete" library. 

"Tried and tested by time" doesn't mean much, actually. 
https://www.youtube.com/watch?v=JFdvFSFO4ho 1:02:20





Making RaspberryPi appliances safely with Go -- `gokrazy` \
`Michael Stapelberg: Why I wrote my own rsync` \
https://www.youtube.com/watch?v=wpwObdgemoE





`switch joycon controllers on linux` \
https://www.reddit.com/r/linux_gaming/comments/fxwh54/using_nintendo_switch_controllers_on_linux/


# Linux USB /dev/input/ fun

- https://www.google.com/search?q=linux+disable+controller+device+command+line&client=firefox-b-d&ei=eiueY6SzEpO-xc8P9sKd8Ag&ved=0ahUKEwik2KvXw4H8AhUTX_EDHXZhB44Q4dUDCA4&uact=5&oq=linux+disable+controller+device+command+line&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQAzIFCAAQogQyBQgAEKIEMgUIABCiBDIFCAAQogQyBQgAEKIEOgoIABBHENYEELADOgYIABAHEB5KBAhBGABKBAhGGABQ4AlYwRVg6BZoAXABeAGAAWqIAbUHkgEDOS4ymAEAoAEByAEIwAEB&sclient=gws-wiz-serp
- https://steamcommunity.com/app/286690/discussions/0/619573787388839852/?l=spanish
- https://bbs.archlinux.org/viewtopic.php?id=193413
- https://ubuntuforums.org/showthread.php?t=1663037
- https://ubuntuforums.org/showthread.php?t=2186521


# Hardware Repair

## Batteries w/ 3 wires (black&red + white / yellow extra)

Research: 

- https://electronics.stackexchange.com/questions/152053/replace-a-three-wire-tablet-battery-with-a-two-wire-one
- https://www.google.com/search?q=polymer+battery+white+wire
- https://www.large.net/news/8wu43kx.html

Buy: 
- https://cz.rs-online.com/web/p/dobijitelne-baterie-specialnich-velikosti/1449405 
