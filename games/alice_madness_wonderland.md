
# Steam Deck Shrink Black Screen

[Steam discussions post](https://steamcommunity.com/app/19680/discussions/0/3269059787433104033/)

When you use the shrink ability and have Post Processing turned on, 
the screen will turn black after a few uses of shrink.

But the game looks horrible without Post Processing!

To fix it properly you need to turn off full screen mode -- but there is no option 
for it in the game, gotta go change the configs...

- open Steam in Desktop mode and locate Alice Madness Returns
- right click the game and select Manage > Browse local files option
- open `/Engine/Config/BaseEngine.ini`
- find `Fullscreen=True` -- change to `False`
- go back to Game mode and play Alice Madness Returns
