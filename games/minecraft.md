# Managing versions - https://multimc.org/


# Networking on LAN

## One copy of the game -> One person online, one offline
- Offline has to have a name different from online person
- Offline name is used as PlayerID -- must remain the same
  - with the most recent version of *MultiMC*, the name remains after restarts

## Setting up peer-to-peer LAN
= Just between devices, no router/switch (tethering, shared network)

> Unless using 2 network devices (wired WAN, wifi LAN or vice versa; 
> or a phone tether to provide WAN through USB)
> the network will not have access to internet. \
> Minecraft then won't connect to any `online_mode` servers 
> because it wants to authenticate with Mojang 
> -- you need internet for functional client-client LAN. 

> Also, the server won't show up in the list automatically -- you will
> have to copy the IP from CLI `ip a | grep inet`, add the port that 
> MC reports into the chat and do a *Direct connection*. 

**In most GUIs**
- create new connection
- WIFI/wired **(shared)**
- leave the defaults, maybe set names and passwords
- done. connect other computers

## LAN, but no internet
- Minecraft run from the usual client runs the server in "online mode". 
- Thus, all parties must be authenticated with the Minecraft Auth 
  and be online in order to play. 
  - Results in: *the authentication servers are currently not reachable*
  - ...but could be any text on other versions -- this is 1.18

- Supposedly, one could...
  - fire up the dedicated server
  - set `online_mode false` option
  - [source](https://www.minecraftforum.net/forums/support/server-support-and/2883877-play-on-lan-no-internet-connection-at-all-offline)
  - Which is pretty dumb when it can be so easy to do. 


## Running the dedicated server
> MultiMC does not do dedicated servers. We must do it outside, despite
> needing the modlist and so on. 

> **I am not clear if it is worth the trouble with keeping yet another
> copy of the modlist and the potential issues with setting this all up.
>
> _So it's not finished yet -- we ended up not using this at all._ 
>
> If you really need it, just use a wired connection between the PCs
> and keep access to the internet via wifi on the devices. Or do some
> phone tethering or something.**

### Copy the modlist from client, used mods from the MultiMC instance

### Download the server (fabric-specific)

- This is bound by the modding wrapper used. 
  - We are using **Fabric** with 1.18
  - Get the server runner here: https://fabricmc.net/use/server/
  - Follow the **Server (simple method)** here: https://fabricmc.net/wiki/install

### Run the server


# Mods

## Necessities and Tweaks 

### Graves
### Tweaks
- 1.18: Charm
- 1.12: Quark

### Longer days

```
Setup:

While in game use /taw set-cycle-length <dimension> <day length> <night length>

For example /taw set-cycle-length minecraft:overworld 24000 9000 will set day duration to 24000 ticks and night to 9000 ticks for dimension overworld.

If you want to remove configuration for world use /taw remove-cycle-entry <dimension>

World id can be obtained using /taw get-current-world-id, this command will print id of current world you are in

You can also navigate to config/time-and-wind/time-data.json and make changes there.

After all changes made use /taw reload to apply them
```


## Cooking
..is fucked

## Technicals (IC2 and so on)

### IC2
For **1.12**, IC2 is great

For **1.18**, IC2 is dead
- Almost dead, someone is trying to port \
  http://jenkins.ic2.player.to/job/IC2/job/1.18/21/
- Here's some discussion: \
  https://www.reddit.com/r/feedthebeast/comments/of1upv/modern_replacement_of_industrial_craft_2/

### Tech Reborn 
Pretty much an IC2 ripoff, I like it. 

[Wiki](https://wiki.techreborn.ovh/doku.php)

### Mekanism
Tries to do too much 
- voice server? Why does it have that
- "osmium"
- baby creepers

