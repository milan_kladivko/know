

# Common hardware-related issues
> Usually a bad job at soldering correctly etc. 

`Key presses are repeated randomly -- chatter.`  \ 
https://deskthority.net/wiki/Chatter
- Just redo any solder points related to the key. That includes
  the diodes too -- especially if they're SMD. 


# Some improvements and userspace libraries / patches

`Achordion for Enhanced Hold-Taps`  \
https://getreuer.info/posts/keyboards/achordion/index.html#explanation



# Home row mods resources 
> These are pretty shit in QMK and there's new developments in this field
> pretty much every time I take a look at it. 
> It's possible that new stuff makes it more viable than hacking 
> in the config numbers. 

https://precondition.github.io/home-row-mods
 https://github.com/manna-harbour/miryoku_qmk/blob/miryoku/users/manna-harbour_miryoku/config.h
 https://www.reddit.com/r/ErgoMechKeyboards/comments/tiejpp/home_row_mods_what_works_for_you/
 




# Common QMK issues 


## Ignore combos when on a layer 

https://www.reddit.com/r/olkb/comments/iavl2n/how_do_i_turn_off_combos_on_the_default_layer_and/
```
layer_state_t layer_state_set_user(layer_state_t state) {
    combo_disable();
    if (layer_state_cmp(state, _FROG)) {
        combo_enable();
    }
    return state;
};
```



## QMK doctor
Tells us many useful things that should be taken care of.


## "We do not recommend avr-gcc newer than 8."

- >= 12 straight up fails to build sometimes. 
- `avr-gcc` is used for compiling for AVR-architecture CPUs -- it doesn't have much use outside of QMK
  - `pacman -Qi avr-gcc` to check


- QMK has an actual install script in its `util` folder where it 
  installs the dependencies correctly. 
  - The upfront docs are lying. 
  - We need a specific version of `avr-gcc @ 8` not newer so just dumping
    the dependencies into the package manager of the distro is not enough. 
    - Another point against distro-based dependencies for these things... 
      Thank god I'm not a person that needs to compile anything else on AVR!
  - Here's the installer code: 
```sh 
## ... from /qmk_firmware/util/install/arch.sh ...
#!/usr/bin/env bash

_qmk_install() {
    echo "Installing dependencies"

    sudo pacman --needed  --noconfirm -S \
        base-devel clang diffutils gcc git unzip wget zip python-pip \
        avr-binutils arm-none-eabi-binutils arm-none-eabi-gcc \
        arm-none-eabi-newlib avrdude dfu-programmer dfu-util \
        riscv64-elf-binutils riscv64-elf-gcc riscv64-elf-newlib
    sudo pacman --needed --noconfirm -U https://archive.archlinux.org/packages/a/avr-gcc/avr-gcc-8.3.0-1-x86_64.pkg.tar.xz
    sudo pacman --needed --noconfirm -S avr-libc # Must be installed after the above, or it will bring in the latest avr-gcc instead

    sudo pacman --needed  --noconfirm -S hidapi  # This will fail if the community repo isn't enabled

    python3 -m pip install --user -r $QMK_FIRMWARE_DIR/requirements.txt
}
## ... from /qmk_firmware/util/install/arch.sh ...
```


## Output too big for the my board

- Use GCC 8 (but I dunno how to do that, never tried)

- Disable features in `rules.mk` -- some may be enabled for no reason. 
  And there might be debugging information in your binary too. 
```
Optimizing the compile size
https://github.com/qmk/qmk_firmware/issues/3224
https://thomasbaart.nl/2018/12/01/reducing-firmware-size-in-qmk/

EXTRAFLAGS  += -flto

NO_ACTION_MACRO = yes
NO_ACTION_FUNCTION = yes
DISABLE_LEADER = yes

COMMAND_ENABLE = no
UNICODE_ENABLE = no
```


## Fails to commit internals
- https://github.com/qmk/qmk_firmware/issues/15476
  - `platforms/avr/drivers/i2c_master.c`
  - `qmk clean -a`

- "TWCR" type issues
  - https://www.reddit.com/r/ErgoMechKeyboards/comments/uqcffw/cant_compile_qmk_for_my_dactyl_manuform/i8qbn7q/
  - `avr-gcc == 12` actually breaks QMK now -- time to do a GCC downgrade!! 
  - The doctor warns us, but now it's making a difference! 
  - Look at the install scripts in `/qmk_firmware/util/install/*.sh`
    and do that instead of the github readme -- it's lying. 

## `qmk clean -a`
- after every `gcc` update (== system update)
- just in case with compile errors

